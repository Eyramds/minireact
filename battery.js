const getBattery = navigator.getBattery().then(function(battery) { 
  
    let isCharging = battery.charging ? "En charge" : " "
    let level = battery.level * 100 + "%"
    let dischargingTime = Math.round(battery.dischargingTime / 3600) + " heures"

    console.log(battery)
    
    battery.addEventListener('chargingchange', function() { 
      isCharging = battery.charging ? "En charge" : " "; 
    }); 
    
    battery.addEventListener('levelchange', function() { 
      level = battery.level * 100 + "%"; 
    }); 
    
    battery.addEventListener('dischargingtimechange', function() { 
        dischargingTime = Math.round(battery.dischargingTime / 3600) + " heures"
    }); 

    let chargingText = document.createElement("p");
    let levelText = document.createElement("p");
    let dischargingTimeText = document.createElement("p");
    document.body.appendChild(chargingText)
    document.body.appendChild(levelText)
    document.body.appendChild(dischargingTimeText)
    chargingText.innerHTML = isCharging
    levelText.innerHTML = "Niveau de batterie : " + level
    dischargingTimeText.innerHTML = "Il vous reste une autonomie de " + dischargingTime
    
  });
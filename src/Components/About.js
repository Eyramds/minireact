import {Component} from "./Component.js";

export class About extends Component {
    constructor() {
        super({name: { type: "string", enum: ["world", "you", "me"] },});
    } 

    render() {
        return React.createElement("div", { toWhat: { name: this.props.name } }, [
            "Hello {{toWhat.name}}",
        ]);
    }
}
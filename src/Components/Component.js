import {ReactDOM} from "../ReactDom.js";

export class Component {
  
    constructor(props) {
      this.props = props;
      this.state  = {};
    };

    setState(state) {
      this.state = { ...this.state, ...state};
      if(needUpdate ())
      {
        shouldUpdate();
      }
    };
  
    setProps(props) {
      this.props = props;
      if(needUpdate ())
      {
        shouldUpdate();
      }
      
    };
    receiveData(props) {
      this.prevProps = Object.assign({}, this.props);
      this.prevState = Object.assign({}, this.state);
      setProps(props);
    };
  
    display(props) {
      receiveData(props);
      if(this.needUpdate()) {
        shouldUpdate(this.render());
      };
    };
  
    needUpdate() {
      return ((JSON.stringify(this.prevProps) !== JSON.stringify(this.props) || (JSON.stringify(this.state) !== JSON.stringify(this.prevState)) ));
    };
  }

  function shouldUpdate(newDom) {
    // const activeEltId = document.activeElement ? document.activeElement.id : null;
  
    while (ReactDOM.root.hasChildNodes()) {
      ReactDOM.root.removeChild(ReactDOM.root.lastChild);
    }

    ReactDOM.render(newDom, ReactDOM.root);
  }

export const ReactDOM = {

    root : document.getElementById('root'),

    render(rElement, hElement) {
        hElement.appendChild( typeof (rElement) === 'object' ? rElement.render() : rElement);
    }
};
import {Component} from "./Components/Component.js";
import {Home} from "./Components/Home.js";
import {About} from "./Components/About.js";
import {ReactDOM} from "./ReactDom.js";
import {React} from "./React.js";

export class Router extends Component {
  state = {
    path: "/home",
    root: "/"
  };

  render() {
    return React.createElement("div", {}, [
      React.createElement(
        "button",
        { onClick: () => this.setState({ path: "/home" }) },
        ["Home"]
      ),
      React.createElement(
        "button",
        { onClick: () => this.setState({ path: "/about" }) },
        ["About"]
      ),
      path === "/" && React.createElement(root),
      path === "/home" && React.createElement(Home)
    ]);
  }

  // cette fonction est à appeler qlq part
  reRender(element){
    let root;
    element.children.forEach(
      root.appendChild(childElement)
    )
    return root;
  }
}
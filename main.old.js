export * from './src';

let ReactDOM = {
  render(rElement, root) {
    root.appendChild(rElement);
  },
};

let React = {
  createElement(tagOrComponent, props, children) {
    let element;
    if (tagOrComponent === "div") {
      element = document.createElement(tagOrComponent);
      for (let attribute in props) {
        element.setAttribute(attribute, props[attribute]);
      }
      for (let subElement of children) {
        if (typeof subElement === "string")
          subElement = document.createTextNode(
            subElement /**.interpolate(props) */
          );
        element.appendChild(subElement);
      }
    } /** component **/ else {
      if (!type_check(props, tagOrComponent.propTypes)) throw new TypeError();
      return tagOrComponent.display(props);
    }

    return element;
  }//trigger rerenderdom, 
};


class Component {
  
  state = {};
  constructor(props) {
    this.props = props;
  }

  receiveData(data) {
    //propaccess
    this.prevProps = Object.assign({}, this.props);
    this.props.state = data.state;
    this.props.origin = data.origin;
    this.props.position = data.position;
  };

  display(props) {
    receiveData(props);
    if(this.needUpdate()) {
      return this.render();
    };
  }

  render() {
      switch (this.props.state) {
        case "updated":
          return this;
        case "deleted":
          return false
        default:
            this.props = prevProps;
          return this;
      } 
  }

  needUpdate () {
    return (JSON.stringify(this.prevProps) !== JSON.stringify(this.props)) || (JSON.stringify(this.state) !== JSON.stringify(this.props.state));
  }
  
  prop_access(props) {

    if (this === "undefined" || !this || typeof props !== "string" || !props) {
      return this;
    }
   
    let path = "";

    for (const prop of props.split(".")) {
      path =  `.${prop}`;

        if (!this[prop]) {
            console.log(`${path} doesn't exist`);
            break;
        }
        else if(this[prop] === null || this[prop] === "" ) {
          return this;
        }
        
        this = this[prop]
    }
    return this;
  }

  prop_accessV1(obj, props) {

    if (obj === "undefined" || !obj || typeof props !== "string" || !props) {
      return obj;
    }
   
    let path = "";

    for (const prop of props.split(".")) {
        path =  `.${prop}`;

        if (!obj[prop]) {
            console.log(`${path} doesn't exist`);
            break;
        }
        else if(obj[prop] === obj || obj[prop] === "" ) {
          return obj;
        }
        
        obj = obj[prop]
    }
    return obj;
  }

}

class todoList extends Component {
  propTypes = {
    name: { type: "string", enum: ["world", "you", "me"] },
  };

  render() {
    return React.createElement("div", { toWhat: { name: this.props.name } }, [
      "Hello {{toWhat.name}}",
    ]);
  }
}

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: props.defaultValue || 0,
    };
  }

  propTypes = {
    defaultValue: { type: "number" },
  };

  get render() {
    return React.createElement("div", {}, [
      React.createElement(
        "button",
        { onClick: () => this.setState({ counter: this.state.counter + 1 }) },
        ["Add"]
      ),
      React.createElement("span", { title: this.state.counter }, ["{{title}}"]),
    ]);
  }
}

class Router extends Component {
  state = {
    path: "/home",
  };

  render() {
    return React.createElement("div", {}, [
      React.createElement(
        "button",
        { onClick: () => this.setState({ path: "/home" }) },
        ["Home"]
      ),
      React.createElement(
        "button",
        { onClick: () => this.setState({ path: "/about" }) },
        ["About"]
      ),
      path === "/home" && React.createElement(Home),
      path === "/about" && React.createElement(About),
    ]);
  }
}

ReactDOM.render(
  React.createElement("div", { toWhat: { name: "World" } }, [
    "Hello {{toWhat.name}}",
    React.createElement(todoList, { name: "world" }),
    React.createElement(Counter, { defaultValue: 10 }),
    React.createElement(Counter, { defaultValue: 0 }),
  ]),
  document.getElementById("root")
);



//<=>
//<div>
//  Hello World
//  <div>
//    Hello World
//  </div>
//  <div>
//    <button>Add</button>
//    <span>10</span>
//  </div>
//  <div>
//  <button>Add</button>
//    <span>0</span>
//  </div>
//</div>

//I] Pros : generation, Cons: Update
//  React.createElement => DomElement
//  Component.render => DomElement
//  ReactDOM.render => rootElement.appendChild(DomElement);
//
//II] Pros: Update, Cons: generation
//  React.createElement => Object
//  Component.render => Object
//  ReactDOM.render =>
//    1) Object => DomElement
//    2) rootElement.appendChild(DomElement);


export function type_check_v1(variable, type){

  switch(typeof variable) {
      case "number":
      case "symbol":
      case "string":
      case "boolean":
      case "undefiened":
      case "function":
          return type === typeof variable;
      case "object":
          switch (type){
              case "null" :
                  return variable === null;
              case "array":
                  return Array.isArray(variable);
              default:
                  return variable !== null && !Array.isArray(variable);
          }
  }
}

export function type_check_v2(variable, conf) {

  for (toCheck in conf) 
      switch(toCheck) {
          case "type":
              if(type_check_v1(variable,conf.type) === false) return false;
          case "value":
          case "enum":
              return type === typeof variable;
          case "object":
              switch (type){
                  case "null" :
                      return variable === null;
                  case "array":
                      return Array.isArray(variable);
                  default:
                      return variable !== null && !Array.isArray(variable);
              }
      }
}

export function type_check(variable, conf) {
  for (toCheck in conf) {
      switch (toCheck) {
          case "type":
              if (type_check_v1(variable, conf.type) === false) return false;
              break;
          case "value":
              if (JSON.stringify(variable) !== JSON.stringify(conf.value))
                  return false;
              break;
          case "enum":
              let found = false;
              for (subValue of conf.enum) {
                  found = type_check_v2(variable, { value: subValue });
                  if (found) break;
              }
              if (!found) return false;
              break;
          case "properties":
              for (prop in toCheck) {
                  switch (prop) {
                      case "type":
                          if (type_check_v1(variable, conf.type) === false) return false;
                          break;
                      case "value":
                          if (JSON.stringify(variable) !== JSON.stringify(conf.value))
                              return false;
                          break;
                      case "enum":
                          let found = false;
                          for (subValue of conf.enum) {
                              found = type_check_v2(variable, { value: subValue });
                              if (found) break;
                          }
                          if (!found) return false;
                          break;
                  }
              }
      }
  }
  return true;
}



const getWeatherData = async function() {

    let cityChoice = 'paris'
    
    let response = await fetch("https://api.openweathermap.org/data/2.5/weather?q="+cityChoice+"&appid=d94ce319c7114d95efa3d608c2f85a20")
    let data = await response.json()

    let city = await data.name
    
    let weather = data.weather
    let descriptionWeather = weather[0].description

    let main = data.main
    let tempe = Math.round((main['temp']) - 273.5)

    let iconcode = weather[0].icon
    let iconurl = "http://openweathermap.org/img/w/" + iconcode + ".png";


    //creating Boostrap
    const card = document.createElement('div')
    card.className = 'card p-4';

    const flex = document.createElement('div')

    flex.className = 'd-flex';
    
    const flex2 = document.createElement('div')
    
    flex2.className = 'd-flex';

    let title = document.createElement('h6')
    title.className = "flex-grow-1";

    let clock = document.createElement('h6')
    
    let tempcontainer = document.createElement ('div')
    tempcontainer.className = "d-flex flex-column temp mt-5 mb-3";

    let heading = document.createElement('h1')
    heading.className = 'mb-0 font-weight-bold';
    heading.id = 'heading';

    let description = document.createElement('span')
    description.className = "small grey";

    //creation du formulaire
    //creating constants
    const base = document.querySelector('body');
    const container = document.createElement('div');
    const form = document.createElement('form');
    const formGroup = document.createElement('div');
    const input = document.createElement('input');
    const button = document.createElement('button');
   
    //adding boostrap classes
    container.className = 'container';
    formGroup.className = 'form-group';
    input.setAttribute('type', 'text');
    input.className = 'form-control';
    input.id = 'city';
    input.defaultValue = 'Country';
    button.setAttribute('type', 'button');
    button.textContent = 'Check!';
    button.classList = 'btn btn-primary';
    button.id = 'btn';

    
    // adding child
    base.appendChild(container);
    container.appendChild(form);
    form.appendChild(formGroup);

    container.appendChild(card);
    card.appendChild(flex);

    flex.appendChild(title);
    title.innerHTML = city


    var d = new Date();
    flex.appendChild(clock)
    clock.id="clock"

    card.appendChild(tempcontainer);
    tempcontainer.appendChild(heading);
    heading.innerHTML = tempe + "°C";

    tempcontainer.appendChild(description);
    description.innerHTML = descriptionWeather

    flex.appendChild(clock)
    document.getElementById("clock").innerHTML = d.getHours() +":"+  d.getMinutes();

    let newImg = document.createElement("img")

    card.appendChild(flex2);
    flex2.appendChild(newImg)
    newImg.setAttribute("src", iconurl)
    newImg.setAttribute("width", "120px");

}


getWeatherData()